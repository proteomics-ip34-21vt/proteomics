# Usage

It is assumed that the `docker` command is available and the user is familiar with the basics of
[docker run](https://docs.docker.com/engine/reference/run/).

## Local Files

Assuming in your working directory is the data from PRIDE experiment [PXD024072](https://www.ebi.ac.uk/pride/archive/projects/PXD024072)
in two folders `group1` and `group2` so that `PXD024072` is the top folder.

After running the following command (and it runs through successfully) the output
files are found in your working directory, in a folder called `out`.

The `user` switch is so the container does not run as root (for security reasons and also
so the output files have the proper permissions).

The folder mounted to `/proteomics-state` needs to have the proteomes database file
(`proteomes-all.tab`) inside.

Download the file here: https://www.uniprot.org/proteomes/ (`Download -> Go`) or with the
[direct link](https://www.uniprot.org/proteomes/?query=*&format=tab&force=true&columns=id,name,organism-id,proteincount,busco,cpd,genome-assembly-representation).

Take note that the mounted `work` folder can grow in size very fast, depending on experiment sizes. It is safe to delete
after runs.

```
docker run -it \
    --user $(id -u):$(id -g) \
    -v "$PWD":/data \
    -v "$PWD":/proteomics-state \
    -v "$PWD/work":/tmp/work \
    cr.gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/docker/proteomics:1.0.0 \
    proteomics local \
    --name test01 \
    --ram 12G \
	--downstream \
    --output_dir /data/out/ \
    --input_dir /data/PXD024072/ \
    --species_id 10090 \
    --workflow labelfree
```

## PRIDE Experiments

Download the `proteomes-all.tab` as described in the `Local Files` part.
The docker command works exactly the same as described there as well.

What changes is that the experiment data is now downloaded automatically
from the PRIDE database.

The following command runs the same experiment as above.

```
docker run -it \
    --user $(id -u):$(id -g) \
    -v "$PWD":/data \
    -v "$PWD":/proteomics-state \
    -v "$PWD/work":/tmp/work \
    cr.gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/docker/proteomics:1.0.0 \
    proteomics pride \
    --name test01 \
    --ram 12G \
	--downstream \
    --output_dir /data/out/ \
	--grouping_method simple \
	--default_quantification labelfree \
	--keywords PXD024072
```
