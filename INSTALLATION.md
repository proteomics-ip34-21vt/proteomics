# Installing Proteomics

## Docker

### Prerequisites

The container engine [docker](https://docs.docker.com/engine/install/) has to be installed. How exactly
varies between operating systems and even within them (the usage instructions assume you are on a \*nix-like system,
eg. linux, wsl2, macOS).

For the following steps it is assumed that the `docker` command is available in the terminal of your choice.

### Configuration

Follow the steps in [creating a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)
while logged in with your account on [gitlab.fhnw.ch](https://gitlab.fhnw.ch). Under `scopes` only check `read_registry`.

Make a note of the name you gave as well as the token (it will only be shown once).

In your terminal log in to the gitlab container registry:

```
docker login -u <token-name> -p <token> cr.gitlab.fhnw.ch
```

With the message `Login Succeeded`, the configuration is done.

### Installation

Pull the image (the version number at the end is subject to change):

```
docker pull cr.gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/docker/proteomics:1.0.0
```

Check if proteomics can start:

```
docker run -it cr.gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/docker/proteomics:1.0.0 proteomics --help
```

If something similar to the following is shown, the installation was successful:

```
usage: proteomics [-h] [-v] {minimal,local,pride} ...

positional arguments:
  {minimal,local,pride}
    minimal             Protemics minimal initialization
    local               Proteomics local interface
    pride               Protemics pride interface

options:
  -h, --help            show this help message and exit
  -v, --version         Prints out the current version
```
