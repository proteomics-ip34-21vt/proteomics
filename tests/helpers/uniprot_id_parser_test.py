"""Tests the uniprot_id_parser class."""

from peewee import SqliteDatabase
from proteomes_converter.convert import init_db
from proteomes_converter.models import CPD, GenomeRepresentation, Proteomes, db

from proteomics.helpers import uniprot_id_parser


def setup_memory_db() -> None:
    """Set up a test database in memory."""
    db.initialize(SqliteDatabase(":memory:"))
    init_db()

    cpd = CPD(name="Unknown").save()
    refseq = GenomeRepresentation(name="full").save()

    Proteomes(
        proteome_id="UP000003056",
        organism="candidate division TM7 genomosp. GTL1 (Strain: GTL1)",
        organism_id=443342,
        protein_count=670,
        busco="C:20.2%[S:19.4%,D:0.8%],F:4%,M:75.8%,n:124",
        cpd=cpd,
        genome_representation=refseq,
    ).save()
    Proteomes(
        proteome_id="UP000594129",
        organism="crAssphage cr131_1",
        organism_id=2772093,
        protein_count=85,
        busco="",
        cpd=cpd,
        genome_representation=refseq,
    ).save()
    Proteomes(
        proteome_id="UP000293802",
        organism="bacterium",
        organism_id=1869227,
        protein_count=5998,
        busco="C:93.5%[S:93.5%,D:0%],F:2.4%,M:4%,n:124",
        cpd=cpd,
        genome_representation=refseq,
    ).save()


def test_from_orga_id() -> None:
    """Test valid and invalid organism_id's."""
    setup_memory_db()

    assert uniprot_id_parser.get_proteome_id_by_orga_id(443342) == "UP000003056"
    assert uniprot_id_parser.get_proteome_id_by_orga_id(2772093) == "UP000594129"
    assert uniprot_id_parser.get_proteome_id_by_orga_id(00) is None


def test_from_orga_name() -> None:
    """Test valid and invalid organism's (by name)."""
    setup_memory_db()

    assert uniprot_id_parser.get_proteome_id_by_orga_name("bacterium") == "UP000293802"
    assert uniprot_id_parser.get_proteome_id_by_orga_name("6293") is None
    assert uniprot_id_parser.get_proteome_id_by_orga_name("no valid name") is None
