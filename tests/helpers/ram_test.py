"""Tests the ram helper package."""
from typing import NamedTuple

from pytest_mock import MockerFixture

from proteomics.helpers import ram


class svmem(NamedTuple):  # pylint: disable=invalid-name
    """Resembles the svmem from the psutil, as it can't be imported directly.

    (from psutil._pslinux import svmem)
    """

    total: float
    available: float
    percent: float
    used: float
    free: float
    active: float
    inactive: float
    buffers: float
    cached: float
    shared: float
    slab: float


def test_installed_ram(mocker: MockerFixture) -> None:
    """Tests if the installed ram is returned correctly."""
    mocker.patch(
        "psutil.virtual_memory", lambda: svmem(64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    )
    assert ram.installed_ram() == 64
