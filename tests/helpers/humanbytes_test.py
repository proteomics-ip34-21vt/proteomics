"""Tests the humanbyte class."""

import pytest

from proteomics.helpers.humanbytes import Humanbytes


def test_to_humanbytes() -> None:
    """Test valid humanbytes conversion."""
    assert Humanbytes.to_humanbytes(0) == "0 Bytes"
    assert Humanbytes.to_humanbytes(1) == "1 Byte"
    assert Humanbytes.to_humanbytes(2) == "2 Bytes"

    assert Humanbytes.to_humanbytes(1024) == "1.00 KB"
    assert Humanbytes.to_humanbytes(1024 * 2) == "2.00 KB"

    assert Humanbytes.to_humanbytes(1024**2) == "1.00 MB"
    assert Humanbytes.to_humanbytes((1024**2) * 2) == "2.00 MB"

    assert Humanbytes.to_humanbytes(1024**3) == "1.00 GB"
    assert Humanbytes.to_humanbytes((1024**3) * 2) == "2.00 GB"

    assert Humanbytes.to_humanbytes(1024**4) == "1.00 TB"
    assert Humanbytes.to_humanbytes((1024**4) * 2) == "2.00 TB"

    assert Humanbytes.to_humanbytes(1177) == "1.15 KB"
    assert Humanbytes.to_humanbytes(1182) == "1.15 KB"
    assert Humanbytes.to_humanbytes(1183) == "1.16 KB"


def test_to_humanbytes_negative() -> None:
    """Test negative humanbytes conversion."""
    with pytest.raises(ValueError, match="Cannot convert negative bytes!"):
        Humanbytes.to_humanbytes(-1)

    with pytest.raises(ValueError, match="Cannot convert negative bytes!"):
        Humanbytes.to_humanbytes(-10)


def test_from_humanbytes_empty() -> None:
    """Test empty humanbytes conversion."""
    msg = "Could not find a matching pattern"

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_humanbytes("")

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_humanbytes("Hello World")


def test_from_humanbytes() -> None:
    """Test valid humanbyte conversions."""
    assert Humanbytes.from_humanbytes("0 Bytes") == 0
    assert Humanbytes.from_humanbytes("1 Byte") == 1
    assert Humanbytes.from_humanbytes("2 Bytes") == 2
    assert Humanbytes.from_humanbytes("3.00 Bytes") == 3
    assert Humanbytes.from_humanbytes("3.40 Bytes") == 3
    assert Humanbytes.from_humanbytes("3.90 Bytes") == 3

    assert Humanbytes.from_humanbytes("1.00 KB") == 1024
    assert Humanbytes.from_humanbytes("2.00 KB") == 1024 * 2

    assert Humanbytes.from_humanbytes("1.00 MB") == 1024**2
    assert Humanbytes.from_humanbytes("2.00 MB") == (1024**2) * 2

    assert Humanbytes.from_humanbytes("1.00 GB") == 1024**3
    assert Humanbytes.from_humanbytes("2.00 GB") == (1024**3) * 2

    assert Humanbytes.from_humanbytes("1.00 TB") == 1024**4
    assert Humanbytes.from_humanbytes("2.00 TB") == (1024**4) * 2

    assert Humanbytes.from_humanbytes("1.15 KB") == 1177
    assert Humanbytes.from_humanbytes("1.151 KB") == 1178
    assert Humanbytes.from_humanbytes("1.1515 KB") == 1178
    assert Humanbytes.from_humanbytes("1.1516 KB") == 1179

    assert Humanbytes.from_humanbytes("1.00KB") == 1024
    assert Humanbytes.from_humanbytes("1.00MB") == 1024**2


def test_from_humanbytes_unknown_unit() -> None:
    """Test unknown units with humanbytes."""
    with pytest.raises(ValueError, match="Unknown unit 'PB'"):
        Humanbytes.from_humanbytes("2PB")

    with pytest.raises(ValueError, match="Unknown unit 'P'"):
        Humanbytes.from_humanbytes("2P")


def test_to_javabytes() -> None:
    """Test to javabytes conversion."""
    assert Humanbytes.to_javabytes(0) == "0"
    assert Humanbytes.to_javabytes(1) == "1"

    assert Humanbytes.to_javabytes(1024) == "1K"
    assert Humanbytes.to_javabytes(1024 * 2) == "2K"

    assert Humanbytes.to_javabytes(1024**2) == "1M"
    assert Humanbytes.to_javabytes((1024**2) * 2) == "2M"

    assert Humanbytes.to_javabytes(1024**3) == "1G"
    assert Humanbytes.to_javabytes((1024**3) * 2) == "2G"

    assert Humanbytes.to_javabytes(1024**4) == "1T"
    assert Humanbytes.to_javabytes((1024**4) * 2) == "2T"

    assert Humanbytes.to_javabytes(1177) == "1K"
    assert Humanbytes.to_javabytes(2047) == "1K"
    assert Humanbytes.to_javabytes(2048) == "2K"


def test_to_javabytes_negative() -> None:
    """Test negative javabytes conversion."""
    msg = "Cannot convert negative bytes!"

    with pytest.raises(ValueError, match=msg):
        Humanbytes.to_javabytes(-1)

    with pytest.raises(ValueError, match=msg):
        Humanbytes.to_javabytes(-10)


def test_from_javabytes_empty() -> None:
    """Test empty javabytes conversion."""
    msg = "Could not find a matching pattern"

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_javabytes("")

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_javabytes("Hello World")


def test_from_javabytes() -> None:
    """Test valid javabytes conversions."""
    assert Humanbytes.from_javabytes("0") == 0
    assert Humanbytes.from_javabytes("1") == 1
    assert Humanbytes.from_javabytes("1B") == 1
    assert Humanbytes.from_javabytes("10B") == 10
    assert Humanbytes.from_javabytes("100B") == 100
    assert Humanbytes.from_javabytes("1023B") == 1023

    assert Humanbytes.from_javabytes("1K") == 1024
    assert Humanbytes.from_javabytes("2K") == 1024 * 2
    assert Humanbytes.from_javabytes("20K") == 1024 * 20

    assert Humanbytes.from_javabytes("1M") == 1024**2
    assert Humanbytes.from_javabytes("2M") == (1024**2) * 2
    assert Humanbytes.from_javabytes("20M") == (1024**2) * 20

    assert Humanbytes.from_javabytes("1G") == 1024**3
    assert Humanbytes.from_javabytes("2G") == (1024**3) * 2
    assert Humanbytes.from_javabytes("20G") == (1024**3) * 20

    assert Humanbytes.from_javabytes("1T") == 1024**4
    assert Humanbytes.from_javabytes("2T") == (1024**4) * 2
    assert Humanbytes.from_javabytes("20T") == (1024**4) * 20


def test_from_javabytes_floating() -> None:
    """Test from javabytes for floating numbers."""
    msg = "Cannot convert floating number of bytes!"

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_javabytes("1.1")

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_javabytes("1.1K")

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_javabytes("1.2K")

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_javabytes("1.3M")

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_javabytes("1.4G")

    with pytest.raises(ValueError, match=msg):
        Humanbytes.from_javabytes("1.5T")


def test_from_javabytes_unknown_unit() -> None:
    """Test from javabytes method for unknown units."""
    with pytest.raises(ValueError, match="Unknown unit 'TB'"):
        Humanbytes.from_javabytes("2TB")

    with pytest.raises(ValueError, match="Unknown unit 'P'"):
        Humanbytes.from_javabytes("2P")
