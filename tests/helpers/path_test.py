"""Test the paths helper package."""

import os

from proteomics.helpers import paths
from proteomics.helpers.paths import PROTEOMES_DB_ENV_VAR, PROTEOMES_DB_FILE_NAME


def test_proteomes_db_path() -> None:
    """Test if the proteomes db path is correctly constructed."""
    old_env_val = None
    if PROTEOMES_DB_ENV_VAR in os.environ:
        old_env_val = os.environ[PROTEOMES_DB_ENV_VAR]
        del os.environ[PROTEOMES_DB_ENV_VAR]

    assert paths.proteomes_db() == os.path.abspath(f"./{PROTEOMES_DB_FILE_NAME}")

    os.environ[PROTEOMES_DB_ENV_VAR] = "/tmp"
    assert paths.proteomes_db() == f"/tmp/{PROTEOMES_DB_FILE_NAME}"

    os.environ[PROTEOMES_DB_ENV_VAR] = ""
    assert paths.proteomes_db() == os.path.abspath(f"./{PROTEOMES_DB_FILE_NAME}")

    if old_env_val is not None:
        os.environ[PROTEOMES_DB_ENV_VAR] = old_env_val
