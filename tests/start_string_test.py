"""Test the stringStarter."""

from proteomics.stringdb_interface.start_string import StringStarter


def try_start_string(input_file: str = "PairwiseComparison.csv") -> None:
    """Possible test-run with PairwiseComparison.csv.

    Args:
        input_file (str, optional): full path to MSStats output .csv file. Defaults to "PairwiseComparison.csv".
    """
    s: StringStarter = StringStarter(species=9606)
    s.make_request(input_file, "start_string_enrichment.xlsx", "start_string_image.png")
