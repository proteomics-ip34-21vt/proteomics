"""Test the string db interface."""
from typing import Set

from proteomics.stringdb_interface.string_interface import StringRequest


def try_string_interface() -> None:
    """Compare the result with.

    https://string-db.org/cgi/input?sessionId=bTQ0cVJpacc8&input_page_active_form=multiple_identifiers

    multiple proteins, example 1
    """
    protein_list: Set[str] = {"trpA", "trpB", "TRPC_ECOLI", "b1263"}
    species: int = 511145
    s: StringRequest = StringRequest()
    params = s.get_params(protein_list, species)
    s.get_enrichment(params=params, write_to_excel=True)
    s.get_image(params=params)
