"""Test conditionGrouping."""

import json
from datetime import datetime
from typing import Tuple

import pytest
from pride_api.pride_objects import PrideFile, PrideParam

from proteomics import condition_grouping as cg
from proteomics.constants import DEFAULT_FILE_ENCODING


def simple_pride_file(name: str) -> PrideFile:
    """Generate a simple pride file only containing a name."""
    return PrideFile(
        project_accessions="",
        accession="",
        file_category=None,
        checksum="",
        public_file_locations=(),
        file_size_bytes=0,
        file_name=name,
        compress=False,
        submission_date=None,
        publication_date=None,
        updated_date=None,
        additional_attributes=(),
        links=(),
    )


@pytest.fixture(name="filenames")  # type: ignore
def fixture_filenames() -> Tuple[PrideFile, ...]:
    """Return a list of filenames to be grouped."""
    return (
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_6.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_5.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_4.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_3.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_2.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_1.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_6.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_5.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_4.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_3.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_2.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_1.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_6.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_5.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_4.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_3.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_2.raw"),
        simple_pride_file("20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_1.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS6.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS5.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS3.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS2.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS1.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr6.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr5.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr4.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr3.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr1.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC6.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC5.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC4.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC3.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC2.raw"),
        simple_pride_file("20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC1.raw"),
        simple_pride_file("this-is-not-relevant.txt"),
    )


@pytest.fixture(name="filenames_grouped")  # type: ignore
def fixture_filenames_grouped() -> Tuple[Tuple[PrideFile, ...], ...]:
    """Return a list of grouped filenames."""
    return (
        (
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_6.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_5.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_4.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_3.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_2.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_INS_1.raw"
            ),
        ),
        (
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_6.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_5.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_4.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_3.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_2.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_CTRL_1.raw"
            ),
        ),
        (
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_6.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_5.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_4.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_3.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_2.raw"
            ),
            simple_pride_file(
                "20180419_QE8_nLC13_SA_ASD_Alba_C2C12_proteome_AIC_1.raw"
            ),
        ),
        (
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS6.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS5.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS3.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS2.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_INS1.raw"
            ),
        ),
        (
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr6.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr5.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr4.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr3.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_Ctr1.raw"
            ),
        ),
        (
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC6.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC5.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC4.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC3.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC2.raw"
            ),
            simple_pride_file(
                "20170814_QE6_HFX_nLC12_ASD_AG_SA_Scerettome_C2C12_AIC1.raw"
            ),
        ),
    )


def test_simple_grouping(
    filenames: Tuple[PrideFile, ...],
    filenames_grouped: Tuple[Tuple[PrideFile, ...], ...],
) -> None:
    """Test the simple grouping strategy."""
    grouped = cg.SimpleGrouping.group(filenames)
    grouped_set = {tuple(x) for x in grouped}
    assert grouped_set == set(filenames_grouped)


def test_lev_grouping(
    filenames: Tuple[PrideFile, ...],
    filenames_grouped: Tuple[Tuple[PrideFile, ...], ...],
) -> None:
    """Test the lev grouping strategy."""
    grouped = cg.LevGrouping.group(filenames, min_group_count=6)
    grouped_set = {tuple(x) for x in grouped}
    assert grouped_set == set(filenames_grouped)


def test_single_grouping(
    filenames: Tuple[PrideFile, ...],
) -> None:
    """Test the single grouping strategy."""
    grouped = cg.SingleGrouping.group(filenames)
    grouped_set = set(grouped[0])
    assert grouped_set == set(filenames[:-1])


def test_flatten(
    filenames: Tuple[PrideFile, ...],
) -> None:
    """Test if grouped returns can be flattened."""
    grouped = cg.SimpleGrouping.group(filenames)
    grouped_tuple = tuple(tuple(x) for x in grouped)
    assert set(cg.ConditionGrouping.flatten(grouped_tuple)) == set(filenames[:-1])


def pride_file_with_filename(filename: str) -> PrideFile:
    """Generate a PrideFile with a given filename.

    Args:
        filename (str): The filename to be set in the PrideFile config

    Returns:
        PrideFile: Pride with with the file_name set
    """
    return PrideFile(
        project_accessions=(),
        accession="",
        file_category=PrideParam(
            param_type="",
            cv_label="",
            accession="",
            name="",
            value=None,
        ),
        checksum="",
        public_file_locations=(),
        file_size_bytes=1000,
        file_name=filename,
        compress=False,
        submission_date=datetime.now(),
        publication_date=datetime.now(),
        updated_date=datetime.now(),
        additional_attributes=(),
        links=(),
    )


def test_save_grouping_to_file_pride_file() -> None:
    """Test the grouped saving mechanism."""
    groups = [
        [
            pride_file_with_filename("f1"),
            pride_file_with_filename("f2"),
            pride_file_with_filename("f3"),
        ],
        [
            pride_file_with_filename("f4"),
            pride_file_with_filename("f5"),
            pride_file_with_filename("f6"),
        ],
    ]
    expected_output = json.dumps(
        [
            [
                "f1",
                "f2",
                "f3",
            ],
            [
                "f4",
                "f5",
                "f6",
            ],
        ]
    )

    grouping_file = cg.ConditionGrouping.save_to_file(groups)

    with open(grouping_file, "r", encoding=DEFAULT_FILE_ENCODING) as fh:
        actual_output = fh.read()

    assert expected_output == actual_output


def test_save_grouping_to_file_str() -> None:
    """Test the grouped saving mechanism."""
    groups = [
        [
            "f1",
            "f2",
            "f3",
        ],
        [
            "f4",
            "f5",
            "f6",
        ],
    ]
    expected_output = json.dumps(
        [
            [
                "f1",
                "f2",
                "f3",
            ],
            [
                "f4",
                "f5",
                "f6",
            ],
        ]
    )

    grouping_file = cg.ConditionGrouping.save_to_file(groups)

    with open(grouping_file, "r", encoding=DEFAULT_FILE_ENCODING) as fh:
        actual_output = fh.read()

    assert expected_output == actual_output
