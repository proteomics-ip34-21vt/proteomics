# Proteomics

[![pipeline status](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/proteomics/badges/sprint/pipeline.svg)](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/proteomics/-/commits/dev)
[![coverage report](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/proteomics/badges/sprint/coverage.svg)](https://gitlab.fhnw.ch/ip34-21vt/ip34-21vt_proteomics/proteomics/-/commits/dev)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

## Overview

![Proteomics Services](./diagrams/out/Proteomics%20Services.svg)

## Development

Requirements:

- Poetry
- pre-commit
- (Windows: use WSL2)

After installation, run the following commands to get started:

```bash
poetry install
pre-commit install
```

Enter the virtualenv with `poetry shell` and make sure the scripts are
up to date with `poetry install` (this is needed to get access to
the `proteomics` and `ppp` commands).

## FAQ

### After pulling from the git repo, a package is missing

You will simply need to run `poetry install` again to get the latest packages installed.

### Where is the documentation?

Download the `proteomics-docs` repository and follow the instructions in the readme there.

### I want to only run the software

Take a look at the [installation](./INSTALLATION.md) and [usage](./USAGE.md) documents.
