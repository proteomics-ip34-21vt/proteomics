"""The proteomics helper collection.

The scipts contained within help automatically scrape the contents of the pride database
and evaluation with multiple tools run within different nextflow workflows.
"""
import logging
from typing import List

__all__: List[str] = ["List", "logging"]


# Set default logging handler to avoid "No handler found" warnings.
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Logging format
formatter = logging.Formatter("%(asctime)s: %(levelname)s - %(name)s - %(message)s")

# create console handler
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(ch)
