"""A list of constants used throughout the proteomics package."""

DATA_DIRECTORY = "./data"
DEFAULT_FILE_ENCODING = "UTF8"
VERSION = "0.0.0"
