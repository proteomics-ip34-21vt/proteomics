"""Flag all proteins which are found in given fasta file."""
import logging
import os
from typing import List

import pandas as pd
from Bio import SeqIO

logger = logging.getLogger(__name__)


class FastaProteinFinder:
    """Class to find proteins in given fasta file."""

    def __init__(self, protein_tsv: str, fasta_file: str) -> None:
        """Init FastaFinder with a protein.tsv and a db.fasta file.

        Args:
            protein_tsv (str): path to your protein.tsv file
            fasta_file (str): path to your custom fasta file
        """
        self.fasta: List[str] = []
        self.fasta_file = os.path.abspath(fasta_file)
        self.protein_tsv = os.path.abspath(protein_tsv)

    def _load_fasta_file(self) -> None:
        """Load all protein ids from a fasta file and save it in self.fasta."""
        logger.info("Load protein ids from %s", self.fasta_file)
        with open(self.fasta_file, "rt", encoding="utf-8") as fd:
            fasta_sequences = SeqIO.parse(fd, "fasta")
            for fasta in fasta_sequences:
                self.fasta.append(fasta.id)

    def _test_protein(self, protein: str) -> bool:
        """Test if a protein occurs as a substring in self.fasta.

        Args:
            protein (str): Protein Id to look for

        Returns:
            bool: True if found
        """
        for protein_id in self.fasta:
            if protein in protein_id:
                return True
        return False

    def write_xlsx(self, target: str) -> None:
        """Save the flagged protein.tsv as .xlsx.

        Args:
            target (str): The output file. Example: path/to/new/proteins_flagged.xslx
        """
        self._load_fasta_file()
        protein_df = pd.read_csv(self.protein_tsv, delimiter="\t")
        result = []
        logger.info("Compare your protein ids with the fasta file.")
        for protein in protein_df["Protein ID"]:
            result.append(self._test_protein(protein))
        protein_df["in custom fasta"] = result
        logger.info("Write to %s", target)
        protein_df.to_excel(target, index=False)
