"""Proteomics protein flag cli."""

import argparse
import os
from typing import List, Optional

from proteomics.constants import VERSION
from proteomics.protein_flagging.fasta_protein_finder import FastaProteinFinder


def flag_cli(call_args: Optional[List[str]] = None) -> None:
    """Flag all proteins in given protein.tsv which occure in given fasta file.

    Args:
        call_args (Optional[List[str]], optional): Arguments passed by the call. Defaults to None.
    """
    parser = argparse.ArgumentParser(prog="flag_proteins")
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"Proteomics version {VERSION}",
        help="Prints out the current version",
    )

    parser.add_argument(
        "--protein_tsv",
        metavar="protein_tsv",
        type=str,
        required=True,
        help="Path to the protein.tsv file.",
    )

    parser.add_argument(
        "--fasta_file",
        metavar="fasta_file",
        type=str,
        required=True,
        help="Path to the fasta file.",
    )

    parser.add_argument(
        "--output_dir",
        metavar="output_dir",
        type=str,
        required=True,
        help="Existing output directory for the output file: protein_flagged.xlsx",
    )

    args = parser.parse_args(call_args)

    fasta_file = os.path.abspath(args.fasta_file)
    protein_tsv = os.path.abspath(args.protein_tsv)
    target = os.path.abspath(os.path.join(args.output_dir, "protein_flagged.xlsx"))

    fasta_protein_finder = FastaProteinFinder(protein_tsv, fasta_file)
    fasta_protein_finder.write_xlsx(target)


if __name__ == "__main__":
    flag_cli()
