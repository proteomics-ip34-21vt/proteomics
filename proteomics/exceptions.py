"""Collection of possible exceptions in the proteomics package."""


class ProteomicsException(Exception):
    """A general proteomics exception."""
