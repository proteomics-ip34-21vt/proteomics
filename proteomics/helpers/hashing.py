"""Hashing helper."""
import hashlib


class Hashing:
    """Helper classes for hashing."""

    @classmethod
    def get_file_sha1(cls, file_path: str) -> str:
        """Get the sha1 hash of the file_path.

        Args:
            file_path (str): Path to the file to be hashed

        Returns:
            str: SHA1 hash of the file
        """
        h = hashlib.sha1()

        with open(file_path, "rb") as file:
            while True:
                # Reading is buffered, so we can read smaller chunks.
                chunk = file.read(h.block_size)
                if not chunk:
                    break
                h.update(chunk)

        return h.hexdigest()
