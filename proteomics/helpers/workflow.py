"""Lists all the available nextflow workflows."""
import abc
import logging
import os
import re
from dataclasses import dataclass
from typing import Dict, List, Optional, Tuple, Type

from pride_api import pride_objects

from proteomics.exceptions import ProteomicsException
from proteomics.helpers.run_config import RunConfig

logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class GitURLs:
    """A list of Git URLs to the same repo. Each repo represents a link to a nextflow repository."""

    name: str
    """The name of the repository"""

    ssh: str
    """SSH URL"""

    http: str
    """HTTP URL"""

    regex: str
    """Which regex should be matched to use this repository"""


class Workflow(abc.ABC):
    """An interface for workflows."""

    # pylint: disable=deprecated-decorator
    @abc.abstractclassmethod
    def get_git_urls(cls) -> List[GitURLs]:
        """Return all potential git urls of the workflow.

        Returns:
            List[Optional[GitURLs]]: List of the git urls.
        """
        ...

    # pylint: disable=unused-argument
    def __init__(self, phospho_modification: Optional[bool] = False) -> None:
        """Initialize the workflow.

        Args:
            phospho_modification (Optional[bool], optional): Set True if it is an opensearch experiment. Defaults to False.
        """
        self.active_url_: GitURLs

    # pylint: disable=unused-argument
    def set_optional_params(
        self, run_config: RunConfig, optional: Optional[List[str]]
    ) -> None:
        """Set additional params automatically.

        Args:
            run_config (RunConfig): the according runconfig
            optional (Optional[List[str]]): optional arguments
        """
        return

    # pylint: disable=unused-argument
    def check_optional_params(self, run_config: RunConfig) -> bool:
        """Check if params exits which are needed for this specific workflow.

        Args:
            run_config (RunConfig): The RunConfig which should be used.

        Returns:
            bool: Return if all params are set.
        """
        return True

    def check_precons(self, run_config: RunConfig) -> bool:
        """Check if all precons are fullfilled.

        Args:
            run_config (RunConfig): _The according RunConfig.

        Returns:
            bool: Return True if fullfilled.
        """
        return self.check_optional_params(run_config)

    def get_pipeline(self) -> str:
        """Get the .nf filename/gitUrl.

        Returns:
            str: .nf filename/gitUrl
        """
        ...


class LocalWorkflow(Workflow):
    """A local workflow. Special case, because it has not any GitURLs."""

    def get_git_urls(self) -> List[GitURLs]:
        """Return nothing, cause it uses a local .nf file.

        Returns:
            List[GitURLs]: An empty list.
        """
        return []

    def __init__(self, file_name: str = "") -> None:
        """Initialize with a file name.

        Args:
            file_name (str): The filename of your .nf file. Default ""
        """
        super().__init__()
        self.local: str = file_name

    def get_pipeline(self) -> str:
        """Return the local .nf filename.

        Returns:
            str: .nf filename
        """
        return self.local


# pylint: disable=line-too-long
class LabelfreeOrOpenSearch(Workflow):
    """The Labelfree Workflow with or without phospho modification (Open Search)."""

    GIT_URL_LABELFREE: GitURLs = GitURLs(
        name="labelfree",
        ssh="git@gitlab.fhnw.ch:ip34-21vt/ip34-21vt_proteomics/workflows/labelfree.git",
        http="https://gitlab.com/proteomics-ip34-21vt/workflows/labelfree.git",
        regex=r"^.*(label[- ]free|gel.(based|free)|TIC|Spectrum count|Normalized Spectral Abundance Factor - NSAF|Peptide counting|precursor ion$|MaxQuant:LFQ intensity|Relative quantification unit|Data-dependent acquisition|Shotgun proteomics|Identified by peptide fragmentation|base peak intensity chromatogram|spectral count peptide level quantitation|MS2 ion mass tolerance|XIC area|Counts|labelfree).*$",
    )

    GIT_URL_OPEN_SEARCH: GitURLs = GitURLs(
        name="open_search",
        ssh="git@gitlab.fhnw.ch:ip34-21vt/ip34-21vt_proteomics/workflows/open_search.git",
        http="https://gitlab.com/proteomics-ip34-21vt/workflows/open_search.git",
        regex=r"open search|open_search",
    )

    def get_git_urls(self) -> List[GitURLs]:
        """Return both git urls.

        Returns:
            List[GitURLs]: Return the labelfree and opensearch git url.
        """
        return [
            LabelfreeOrOpenSearch.GIT_URL_LABELFREE,
            LabelfreeOrOpenSearch.GIT_URL_OPEN_SEARCH,
        ]

    def __init__(self, phospho_modification: Optional[bool] = False) -> None:
        """Initialize the workflow as labelfree or opensearch if phospho-modification.

        Args:
            phospho_modification (Optional[bool]): Phospho modification. Defaults to False.
        """
        super().__init__()
        if phospho_modification:
            self.active_url_ = LabelfreeOrOpenSearch.GIT_URL_OPEN_SEARCH
        else:
            self.active_url_ = LabelfreeOrOpenSearch.GIT_URL_LABELFREE

    def get_pipeline(self) -> str:
        """Return the Labelfree or Opensearch git url, depending if phospho-modification is used.

        Returns:
            str: the active git url.
        """
        return self.active_url_.http


class TMT(Workflow):
    """The tmt workflow."""

    GIT_URL: GitURLs = GitURLs(
        name="tmt",
        ssh="git@gitlab.fhnw.ch:ip34-21vt/ip34-21vt_proteomics/workflows/tmt.git",
        http="https://gitlab.com/proteomics-ip34-21vt/workflows/tmt.git",
        regex=r"^(tmt( intensity \d+)?|itraq)$",
    )

    ANNOTATION_TYPES: List[str] = [
        "6",
        "10",
        "16",
    ]

    def get_git_urls(self) -> List[GitURLs]:
        """Return the tmt git url.

        Returns:
            List[GitURLs]: The tmt git url.
        """
        return [TMT.GIT_URL]

    def check_optional_params(self, run_config: RunConfig) -> bool:
        """Check if the annotation is specified.

        Args:
            run_config (RunConfig): the according RunConfig

        Returns:
            bool: True if specified.
        """
        options = run_config.nextflow_config.options
        if options is not None:
            annotation = options.get("annotation")
            if annotation is not None:
                logger.info(
                    "Found TMT version: %s",
                    run_config.nextflow_config.options["annotation"],
                )
                return bool(TMT.ANNOTATION_TYPES.__contains__(annotation))
        logger.error(
            "non valid annotation type: available are: %s", TMT.ANNOTATION_TYPES
        )
        return False

    def get_pipeline(self) -> str:
        """Return the name of the activated pipeline.

        Returns:
            str: name of the pipeline
        """
        return str(TMT.GIT_URL.http)

    def identify(self, txt: str) -> Dict[str, bool]:
        """Make a dict of which tmt version were found in the text. Test for TMT-6, TMT-10, TMT-16.

        Args:
            txt (str): input description of project

        Returns:
            Dict[str, bool]: key: tmt version, value: bool if version found
        """
        txt = txt.lower()
        versions_found = [
            re.compile(r"tmt(-| )?0?6+"),
            re.compile(r"tmt(-| )?10+"),
            re.compile(r"tmt(-| )?16+"),
        ]

        tmt_dict: Dict[str, bool] = {}
        for idx, annotation in enumerate(TMT.ANNOTATION_TYPES):
            found_annotation = re.search(versions_found[idx], txt)
            tmt_dict[annotation] = found_annotation is not None
        return tmt_dict

    def set_optional_params(
        self, run_config: RunConfig, optional: Optional[List[str]]
    ) -> None:
        """Identify and set the argument for the tmt annotation file.

        Args:
            run_config (RunConfig): Where to set the argument.
            optional (Optional[List[str]]): str to look for a tmt version

        Raises:
            ProteomicsException: no string to look for a tmt version
            ProteomicsException: if multiple versions were detected
            ProteomicsException: if no version were detected
        """
        if optional is None:
            raise ProteomicsException(
                "Failed to set the annotation for a tmt workflow."
            )
        tmt_dict = self.identify(optional[0])
        tmt_version = ""
        for key, val in tmt_dict.items():
            if val:
                if tmt_version == "":
                    tmt_version = key
                else:
                    raise ProteomicsException("could not identify a unique tmt version")
        if tmt_version == "":
            raise ProteomicsException("could not identify any tmt version")
        run_config.nextflow_config.options["annotation"] = tmt_version


all_workflows: Tuple[Type[LabelfreeOrOpenSearch], Type[TMT], Type[LocalWorkflow]] = (
    LabelfreeOrOpenSearch,
    TMT,
    LocalWorkflow,
)


def find_workflow(name: str, phospho_modification: bool = False) -> List[Workflow]:
    """Map a Pride quantification name to a nextflow workflow repo.

    Args:
        name (str): Pride quantification name
        phospo_modification (bool): Set to True for Opensearch. Default = False

    Returns:
        List[Workflow]: All matching workflows. Returns an empty list if none are found
    """
    results: List[Workflow] = []

    for wf in all_workflows:
        wf_obj = wf()
        for git_url in wf_obj.get_git_urls():
            if re.search(git_url.regex, name, re.IGNORECASE) is not None:
                workflow = wf(phospho_modification)
                workflow.active_url_ = git_url
                results.append(workflow)

    if os.path.isfile(name):
        local_wf: LocalWorkflow = LocalWorkflow(name)
        results = [local_wf]

    return results


def _check_for_phospho(pride_obj: pride_objects.PrideObject) -> bool:
    """Check if any phospho modification is found in a pride project.

    Args:
        pride_obj (pride_objects.PrideObject): the pride project object.

    Returns:
        bool: True if any phospho modification is found.
    """
    for ptm in pride_obj.identified_ptm_strings:
        modification = ptm.name.lower()
        if "phospho" in modification:
            return True
    return False


def find_workflow_for_project(
    project: pride_objects.PrideProject,
) -> List[Workflow]:
    """Map a Pride project to a nextflow workflow repo.

    Args:
        project (pride_objects.PrideProject): Pride project

    Returns:
        List[Workflow]: All matching workflows for all quantification methods. Returns an
                        empty list if none are found.
    """
    results: List[Workflow] = []

    for meth in project.quantification_methods:
        phospho_modification: bool = _check_for_phospho(project)
        results.extend(find_workflow(meth.name, phospho_modification))

    return results
