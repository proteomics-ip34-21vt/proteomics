"""Parses organism name or id to proteome id with lookup DB(uniprot)."""
from typing import Optional, cast

from peewee import fn
from proteomes_converter.models import Proteomes


def get_proteome_id_by_orga_id(organism_id: int) -> Optional[str]:
    """Retreive proteome_id in lookup model from given organism_id.

    Args:
        organism_id (int): the organism's id

    Returns:
        Optional[str]: the proteome_id
    """
    try:
        return cast(
            str,
            list(Proteomes.select().where(Proteomes.organism_id == organism_id))[
                0
            ].proteome_id,
        )

    except IndexError:
        return None


def get_proteome_id_by_orga_name(organism_name: str) -> Optional[str]:
    """Retreive organism_id in lookup model from given organism(name).

    Args:
        organism (str): the organism's scientific name

    Returns:
        Optional[str]: the proteome_id
    """
    try:
        return cast(
            str,
            list(
                Proteomes.select().where(
                    fn.lower(Proteomes.organism) == organism_name.lower()
                )
            )[0].proteome_id,
        )
    except IndexError:
        return None
