"""Nextflow configuration."""
import itertools
from dataclasses import dataclass
from typing import Dict, List, Optional


@dataclass
class NextflowConfig:
    """Nextflow run configuration."""

    ram: str
    """Max ram to be used."""

    options: Dict[str, str]
    """Nextflow pipeline parameters"""

    pipeline: Optional[str] = None
    """Local pipeline file or a git repo url."""

    def get_options(self) -> List[str]:
        """Return all options as a list of strings for subprocess.run.

        Returns:
            List[str]: Arguments for subprocess.run.
        """
        cmd: List[str] = []
        if self.options is not None:
            option_args = [(f"--{key}", value) for key, value in self.options.items()]
            cmd = list(itertools.chain.from_iterable(option_args))
        return cmd
