"""Build and execute the RunConfig."""
import abc
import logging
import os
import subprocess
import sys
from typing import List

from proteomics.exceptions import ProteomicsException
from proteomics.helpers.run_config import RunConfig, RunState
from proteomics.helpers.thermo_converter import ThermoConverter

logger = logging.getLogger(__name__)


class NfCommand(abc.ABC):
    """A command which can be executed."""

    @classmethod
    def get_cmd(cls, run_config: RunConfig) -> List[str]:
        """Build the command for subprocess.

        Args:
            run_config (RunConfig): The RunConfig which should be used.

        Raises:
            ProteomicsException: If RunConfig.workflow is None.

        Returns:
            List[str]: A list of str which can be called by subprocess.run
        """
        if run_config.workflow is None:
            logger.error("NfCommand.get_cmd: No workflow selected for this RunConfig")
            raise ProteomicsException("No workflow selected for this RunConfig.")

        nf_config = run_config.nextflow_config

        db_args = run_config.get_custom_fasta_cmd()
        options = nf_config.get_options()

        return [
            "nextflow",
            "run",
            "--species_id",
            str(run_config.species_id),
            "--input_dir",
            run_config.input_dir,
            "--output_dir",
            run_config.output_dir,
            "--ram",
            nf_config.ram,
            "--db_id",
            run_config.uniprot_id,
            "--downstream",
            str(run_config.downstream),
            *db_args,
            *options,
            run_config.workflow.get_pipeline(),
        ]

    @classmethod
    def convert_runconfig_to_mzml(cls, run_config: RunConfig) -> None:
        """Convert all .raw files in input directory to .mzML.

        Args:
            run_config (RunConfig): The RunConfig which should be used.
        """
        group_dirs = os.listdir(run_config.input_dir)
        for group_dir in group_dirs:
            group_dir_path = os.path.join(run_config.input_dir, group_dir)
            files = os.listdir(group_dir_path)
            for file_name in files:
                ThermoConverter.convert_file(os.path.join(group_dir_path, file_name))

    @classmethod
    def execute_cmd(cls, run_config: RunConfig) -> None:
        """Execute the RunConfig.

        Args:
            run_config (RunConfig): The RunConfig which should be executed.

        Raises:
            ProteomicsException: If RunConfig.workflow is None.
            ProteomicsException: Nextflow is not installed.
            ProteomicsException: Nextflow command is invalid.
        """
        if run_config.state == RunState.CREATED:
            if run_config.workflow is None:
                logger.error(
                    "NfCommand.get_cmd: No workflow selected for %s", run_config.name
                )
                run_config.state = RunState.FAILED

            elif not run_config.workflow.check_precons(run_config):
                logger.error(
                    "Precondition for project: %s, workflow %s not fullfilled.",
                    run_config.name,
                    run_config.workflow.get_pipeline(),
                )
                run_config.state = RunState.FAILED
        if run_config.state != RunState.CREATED:
            logger.warning(
                "The runconfig %s is: %s", run_config.config_id, run_config.state
            )
            return
        logger.info(
            "Runnning the run config %s (%s)", run_config.name, run_config.config_id
        )
        run_config.state = RunState.RUNNING
        cls.convert_runconfig_to_mzml(run_config)

        print(*cls.get_cmd(run_config))

        try:
            logger.info("Start to run project: %s.", run_config.name)
            subprocess.run(cls.get_cmd(run_config), check=True)

        except FileNotFoundError as e:
            run_config.state = RunState.FAILED

            raise ProteomicsException(
                "Could not launch the workflow. Please make sure that nextflow is installed!"
            ) from e

        except subprocess.CalledProcessError as e:
            run_config.state = RunState.FAILED

            raise ProteomicsException("Nextflow failed to execute!") from e

        except KeyboardInterrupt:
            run_config.state = RunState.CANCELLED

            sys.exit(1)
        else:
            run_config.state = RunState.PASSED
