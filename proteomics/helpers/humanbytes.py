"""Helps convert bytes from human readable version to machine and back again."""
import math
import re
from typing import Optional


class Humanbytes:
    """Helps convert bytes from human readable version to machine and back again.

    Understands a more human readable version with TB, GB, MB, ... and also the Java
    equivalent with T, G, M, ...
    """

    KB = 1024
    MB = KB**2
    GB = KB**3
    TB = KB**4

    @classmethod
    def to_humanbytes(cls, b: int) -> str:
        """Return the given bytes as a human friendly KB, MB, GB, or TB string.

        Args:
            b (int): The bytes to be converted into a human readable version

        Raises:
            ValueError: Can't convert negative bytes

        Returns:
            str: A human readable version of the size
        """
        # Check if we have a positive input
        if b < 0:
            raise ValueError("Cannot convert negative bytes!")

        if b >= cls.TB:
            return f"{b/cls.TB:.2f} TB"

        if b >= cls.GB:
            return f"{b/cls.GB:.2f} GB"

        if b >= cls.MB:
            return f"{b/cls.MB:.2f} MB"

        if b >= cls.KB:
            return f"{b/cls.KB:.2f} KB"

        return f"{b} {'Byte' if 1 == b else 'Bytes'}"

    @classmethod
    def from_humanbytes(cls, b: str) -> int:
        """Return the given human bytes as a byte value.

        Args:
            b (str): The bytes to be converted from a human readable version

        Raises:
            ValueError: Could not find a matching pattern
            ValueError: Unknown unit

        Returns:
            int: The number of bytes that was written in the string
        """
        matches: Optional[re.Match[str]] = re.match(r"([\d\.]+) ?([\w]+)", b)
        if matches is None:
            raise ValueError("Could not find a matching pattern")

        num = float(matches.group(1))
        unit: str = matches.group(2)

        if unit == "TB":
            return int(cls.TB * round(num, 12))
        if unit == "GB":
            return int(cls.GB * round(num, 9))
        if unit == "MB":
            return int(cls.MB * round(num, 6))
        if unit == "KB":
            return int(cls.KB * round(num, 3))
        if unit.lower() in ("byte", "bytes"):
            return int(num)

        raise ValueError(f"Unknown unit '{unit}'")

    @classmethod
    def to_javabytes(cls, b: int) -> str:
        """Return the given bytes as a java friendly K, M, G, or T appended string.

        Args:
            b (int): The bytes to be converted into a java readable version

        Raises:
            ValueError: Cannot convert negative bytes

        Returns:
            str: A java readable version of the size
        """
        # Check if we have a positive input
        if b < 0:
            raise ValueError("Cannot convert negative bytes!")

        if b >= cls.TB:
            return f"{math.floor(b/cls.TB)}T"

        if b >= cls.GB:
            return f"{math.floor(b/cls.GB)}G"

        if b >= cls.MB:
            return f"{math.floor(b/cls.MB)}M"

        if b >= cls.KB:
            return f"{math.floor(b/cls.KB)}K"

        return str(b)

    @classmethod
    def from_javabytes(cls, b: str) -> int:
        """Return the given java bytes as a byte value.

        Args:
            b (str): The bytes to be converted into a java readable version

        Raises:
            ValueError: Could not find a matching pattern
            ValueError: Cannot convert floating number of bytes!
            ValueError: Unknown unit

        Returns:
            int: The number of bytes that was written in the string
        """
        # Get the matches
        matches: Optional[re.Match[str]] = re.match(r"([\d\.]+)([A-Za-z]*)", b)
        if matches is None:
            raise ValueError("Could not find a matching pattern")

        # Check if we have a floating number
        try:
            num = int(matches.group(1))

        except ValueError as e:
            raise ValueError("Cannot convert floating number of bytes!") from e

        unit: str = matches.group(2)

        if unit == "T":
            return int(cls.TB * num)
        if unit == "G":
            return int(cls.GB * num)
        if unit == "M":
            return int(cls.MB * num)
        if unit == "K":
            return int(cls.KB * num)
        if unit in ("B", ""):
            return num

        raise ValueError(f"Unknown unit '{unit}'")
