"""Paths for proteomics state files."""

import os

PROTEOMES_DB_ENV_VAR = "PROTEOMICS_PROTEOMES_DB"
PROTEOMES_DB_FILE_NAME = "proteomes-all.tab"


def proteomes_db() -> str:
    """Absolute path for the proteomes database file.

    Read the value of the environment variable PROTEOMICS_PROTEOMES_DB if it exists, else use the current working
    directory and expect a file proteomes-all.tab to be in it in both cases.

    Returns:
        str: Absolute path to the proteomes database file.
    """
    proteomes_db_dir = (
        os.environ.get(PROTEOMES_DB_ENV_VAR)
        if PROTEOMES_DB_ENV_VAR in os.environ
        else "."
    )

    if not proteomes_db_dir:
        proteomes_db_dir = "."

    proteomes_db_path = os.path.join(proteomes_db_dir, PROTEOMES_DB_FILE_NAME)

    return os.path.abspath(proteomes_db_path)
