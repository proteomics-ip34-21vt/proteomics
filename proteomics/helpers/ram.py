"""Return the amount of installed ram."""
import psutil


def installed_ram() -> int:
    """Return the amount of installed ram.

    Returns
    -------
    int
        Amount of ram in GB
    """
    return psutil.virtual_memory().total  # type: ignore
