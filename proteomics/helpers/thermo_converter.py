"""A tool to convert .raw files to .mzML with thermo installed."""
from __future__ import annotations

import os
import subprocess
from pathlib import Path
from typing import Any, Dict, Final, List, Optional, Tuple, cast

from proteomics.exceptions import ProteomicsException


class ThermoConverter:
    """Convert raw files to mzML with the TermoRawParser."""

    RAW_EXTENSION: Final = "raw"

    @classmethod
    def _is_convertable(cls, path: str) -> bool:
        """If the fiven file can be converted to mzML.

        Parameters
        ----------
        path : str
            Path to the file to be checked.

        Returns
        -------
        bool
            If the file can be converted to mzML using ThermoRawFileParser.
        """
        return path.endswith(f".{ThermoConverter.RAW_EXTENSION}")

    @classmethod
    def convert_file(
        cls,
        path: str,
        options: Optional[Dict[str, Any]] = None,
        wait: bool = True,
    ) -> Optional[Tuple[str, Optional[subprocess.Popen[bytes]]]]:
        """Convert a .raw file to a .mzML file using thermo. It will be saved to the same directoy as the input file.

        For a full list of options, see the ThermoRawFilwParser documentation:
        https://github.com/compomics/ThermoRawFileParser

        Args:
            path (str): the path of the .raw file
            options (Optional[Dict[str, Any]]): Any further thermoRawFileParser options. By default None

        Returns:
            None if we did not convert the file, the path to the new file if successfull
        """
        if not cls._is_convertable(path):
            return None

        options_list: List[str] = []

        if options is not None:
            for name, value in options.items():
                options_list.extend([name, str(value)])

        out_file = Path(path).with_suffix(".mzML")
        out_file_str = str(out_file)

        # Check if the file exists
        if out_file.exists():
            # Skip conversion
            return out_file_str, None

        # pylint: disable=consider-using-with
        try:
            process = subprocess.Popen(
                ["thermo", "-i", path, "-b", out_file_str, *options_list]
            )  # nosec
        except FileNotFoundError as e:
            raise ProteomicsException(
                "Please make sure, that you have ThermoRawFileParser installed and aliased as `thermo`!"
            ) from e

        if not wait:
            return out_file_str, process

        cls._wait_for_completed_process(process)
        return out_file_str, None

    @classmethod
    def _wait_for_completed_process(cls, process: subprocess.Popen[bytes]) -> None:
        """Wait for a single process until it's completed. If the exit code is non zero, it will raise an exception.

        Args:
            process (subprocess.Popen[bytes]): The process to be watched.

        Raises:
            ProteomicsException: Thrown with a non zero exit code.
        """
        exitcode = process.wait()

        if exitcode != 0:
            args = cast(List[str], process.args)
            raise ProteomicsException(
                f"Failed to convert the file to mzML using ThermoRawFileParser, exitcode {exitcode} with the arguments: {args}",
            )

    @classmethod
    def convert_files(
        cls,
        paths: List[str],
        options: Optional[Dict[str, Any]] = None,
        concurrent: int = 0,
    ) -> List[str]:
        """Convert any raw files listed in the path to mzML. Keeps any other paths intact.

        For a full list of options, see the ThermoRawFilwParser documentation:
        https://github.com/compomics/ThermoRawFileParser

        Args:
            paths (List[str]): List of paths to files. Raw files will be converted to mzML.
            options (Optional[Dict[str, Any]]): Any further thermoRawFileParser options. By default None.

        Returns:
            List[str]: Paths to the converted files
        """
        out: List[str] = []
        processes: List[subprocess.Popen[bytes]] = []
        running_processes = 0
        if concurrent <= 0:
            concurrent = os.cpu_count() or 10

        for path in paths:
            if (
                conv := cls.convert_file(path, options, wait=(concurrent == 1))
            ) is not None:
                conv_path, process = conv
                out.append(conv_path)
                if process is not None:
                    processes.append(process)
            else:
                out.append(path)

            running_processes += 1
            if running_processes >= concurrent:
                p = cls._block_until_process_free(processes)
                processes.remove(p)
                running_processes -= 1

        # Join everything back to gether
        for p in processes:
            p.wait()
            running_processes -= 1

        # Done
        return out

    @classmethod
    def _block_until_process_free(
        cls, processes: List[subprocess.Popen[bytes]]
    ) -> subprocess.Popen[bytes]:
        """Blocks until a process has completed.

        Args:
            processes (List[subprocess.Popen[bytes]]): The list of processes to monitor

        Returns:
            subprocess.Popen[bytes]: The process that has finished executing.
        """
        while True:
            for p in processes:
                try:
                    p.wait(timeout=0.5)
                    cls._wait_for_completed_process(p)

                    return p
                except subprocess.TimeoutExpired:
                    pass
