"""External database helper."""

import logging
import os
from typing import Callable

from peewee import Proxy, SqliteDatabase
from proteomes_converter.convert import run as proteomes_convert
from proteomes_converter.models import db as proteomes_db

from proteomics.helpers import paths
from proteomics.helpers.hashing import Hashing

logger = logging.getLogger("proteomics.helpers.ext_database")


class ExtDatabase:
    """External database check and creator. to be used with `proteomes_converter` and `taxonomy_converter`."""

    @classmethod
    def check_and_create(
        cls,
        input_file: str,
        converter: Callable[[str, str], None],
        database: Proxy,
        extension: str = "sqlite",
    ) -> str:
        """Check if the database has been converted and if not converts it.

        NOTE: When using the database directly after the conversion, we will be using the
        temporarily named database until the next start of the application.

        Args:
            input (str): The input file to be converted
            converter (Callable[[str, str], None]): Which converter to use for the given file
            database (Proxy): Peewee database proxy
            extension (str): Output file extension. By default "sqlite"

        Returns:
            str: Path to the converted database
        """
        logger.info("Check if proteomes-db is up to date.")
        hashed = Hashing.get_file_sha1(input_file)
        folder = os.path.dirname(input_file)
        output_file = os.path.join(folder, f"{hashed}.{extension}")

        if not os.path.exists(output_file):
            logger.info("Converting external dump file to database: %s", input_file)
            converter(input_file, output_file)

        database.initialize(SqliteDatabase(output_file))
        logger.info("Initialized database for %s: %s", input_file, output_file)

        return output_file

    @classmethod
    def check_and_create_dbs(cls) -> None:
        """Check and create the proteomes database and initialize the connection."""
        ExtDatabase.check_and_create(
            paths.proteomes_db(), proteomes_convert, proteomes_db
        )
