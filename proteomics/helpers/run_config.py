"""Run configuration dataclass."""
from __future__ import annotations

import logging
import uuid
from copy import deepcopy
from dataclasses import dataclass
from enum import Enum
from typing import TYPE_CHECKING, List, Optional, Type

from proteomics.condition_grouping import GROUPING_METHODS, ConditionGrouping
from proteomics.helpers.nextflow_config import NextflowConfig

if TYPE_CHECKING:
    from proteomics.helpers.workflow import Workflow


class RunState(Enum):
    """Defines the possible run states of a RunConfig."""

    CREATED = "CREATED"
    """Just created and has not been run yet."""

    RUNNING = "RUNNING"
    """Is currently being run."""

    PASSED = "PASSED"
    """Successfully finished the run."""

    FAILED = "FAILED"
    """The run failed to completley finish."""

    CANCELLED = "CANCELLED"
    """The run got cancelled while it was running."""

    OBSOLETE = "OBSOLETE"
    """A failed/cancelled test that was re-run."""


@dataclass
class RunConfig:
    """Configuration on what to run in this workflow."""

    config_id: str
    """GUID"""

    state: RunState
    """The state of the run."""

    name: str
    """The name of the evaluation (not unique)."""

    output_dir: str
    """The Directory for the output (should be empty or not created)."""

    input_dir: str
    """The input folder with grouped .raw/.mzML files."""

    species_id: int
    """The taxonomy id of the species."""

    uniprot_id: str
    """Uniprot database ID (example id: UP000005640)"""

    custom_fasta_file: Optional[str]
    """"Custom fasta database file for philosopher."""

    downstream: bool
    """If downstream analyis should be executed, if possible. Grouping is mandatory."""

    keywords: List[str]
    """keywords, used for search in the Pride Database"""

    grouping_method: str
    """Which grouping concept should be used on files frompride projects."""

    workflow: Optional[Workflow]
    """The chosen workflow."""

    default_quantifiaction_method: Optional[str]
    """The chosen workflow."""

    nextflow_config: NextflowConfig
    """Nextflow configuration."""

    remove_input: bool

    def copy(self, mark_obsolete: bool = True) -> RunConfig:
        """Create a deep copy of the config with a new configuration id.

        Args:
            mark_obsolete (bool): If the object to be copied should be marked obsolete. By default True.

        Returns:
            RunConfig: A deeply copied copy of the current run config.
        """
        config = deepcopy(self)
        config.config_id = str(uuid.uuid4())

        if mark_obsolete:
            self.state = RunState.OBSOLETE

        return config

    def get_grouping_method(self) -> Optional[Type[ConditionGrouping]]:
        """Get the grouping method type from a string value.

        Returns:
            Optional[Type[ConditionGrouping]]: The grouping method if we have a match, otherwise None
        """
        try:
            grouping_method_impl: Type[ConditionGrouping] = GROUPING_METHODS[
                self.grouping_method
            ]
            return grouping_method_impl

        except KeyError:
            logger = logging.getLogger(self.__class__.__name__)
            logger.error(
                "The selected grouping method does not exists. Choose one of %s",
                GROUPING_METHODS.keys(),
            )

        return None

    def get_custom_fasta_cmd(self) -> List[str]:
        """Get the custom fasta commands for nextflow.

        Returns:
            List[str]: A list of strings, which can be called.
        """
        db_args: List[str] = []
        if self.custom_fasta_file is not None:
            db_args = ["--custom_fasta", self.custom_fasta_file]
        return db_args
