"""Provide different ways of grouping Pride files."""
import json
import pathlib
import re
import tempfile
from abc import ABC, abstractmethod
from typing import Dict, List, Sequence, Set, Type, Union

import Levenshtein as l
from pride_api.pride_objects import PrideFile

from proteomics.constants import DEFAULT_FILE_ENCODING


class ConditionGrouping(ABC):
    """Interface for the sort classes."""

    accepted_extensions = (".raw", ".mzml", ".zip", ".gz", ".rar")

    @classmethod
    @abstractmethod
    def group(cls, files: Sequence[PrideFile]) -> List[List[PrideFile]]:
        """Make a list of groups of Pride files.

        Args:
            files (PrideFile): Incoming files

        Returns:
            List[List[PrideFile]]: A list of groups of Pride files
        """
        ...

    @classmethod
    def _valid_extension(cls, file: Union[PrideFile, str]) -> bool:
        if isinstance(file, PrideFile):
            file_name = file.file_name
        else:
            file_name = file

        return pathlib.Path(file_name).suffix.lower() in cls.accepted_extensions

    @classmethod
    def flatten(cls, groups: Sequence[Sequence[PrideFile]]) -> List[PrideFile]:
        """Make a list of Pride files out of a list of groups of Pride files.

        Args:
            groups (List[List[PrideFile]]): A list of groups of Pride files

        Returns:
            List[PrideFile]: A list of Pride files
        """
        flat_list: List[PrideFile] = []

        for g in groups:
            flat_list.extend(g)

        return flat_list

    @classmethod
    def save_to_file(cls, groups: Sequence[Sequence[Union[PrideFile, str]]]) -> str:
        """Save the given groups of PrideFiles into a temporary json file and returns the files path.

        Args:
            groups (Sequence[Sequence[PrideFile]]): List of grouped PrideFiles.

        Returns:
            str: Path to the temporary file.
        """
        path = tempfile.NamedTemporaryFile().name  # pylint: disable=consider-using-with

        str_groups: List[List[str]] = []
        for group in groups:
            filenames = []
            for file in group:
                if isinstance(file, PrideFile):
                    filenames.append(file.file_name)
                else:
                    filenames.append(file)

            str_groups.append(filenames)

        json_dump = json.dumps(str_groups)
        with open(path, "w", encoding=DEFAULT_FILE_ENCODING) as fh:
            fh.write(json_dump)

        return path


class SimpleGrouping(ConditionGrouping):
    """Groups the Pride files by their name without numbers."""

    @classmethod
    def group(cls, files: Sequence[PrideFile]) -> List[List[PrideFile]]:
        """Group the Pride files by their name without numbers.

        Args:
            files (Sequence[PrideFile]): List of Pride files

        Returns:
            List[List[PrideFile]]: A list of groups of Pride files
        """
        clean_re = re.compile(r"[0-9]*")

        no_num_names: Set[str] = {clean_re.sub("", x.file_name) for x in files}
        groups: Dict[str, List[PrideFile]] = {x: [] for x in no_num_names}

        for a_file in files:
            key = clean_re.sub("", a_file.file_name)
            if not cls._valid_extension(a_file):
                try:
                    del groups[key]
                except KeyError:
                    pass

                continue

            groups[key].append(a_file)

        return list(groups.values())


class LevGrouping(ConditionGrouping):
    """Groups the Pride files by the similarity of their names."""

    @classmethod
    def group(
        cls,
        files: Sequence[PrideFile],
        ratio: float = 0.8,
        min_group_count: int = 0,
    ) -> List[List[PrideFile]]:
        """Group the Pride files by the similarity of their names.

        Args:
            files (Sequence[PrideFile]): Pride files
            ratio (float, optional): The ratio how similar tow files are. Defaults to 0.8.
            min_group_count (int): The minimum group count to be produced. If 0, only the
                ratio is used. Defaults to 0.

        Returns:
            List[List[PrideFile]]: A list of groups of Pride files
        """
        clean_re = re.compile(r"[0-9]*")
        groups: List[List[PrideFile]] = []

        for a_file in files:
            comp: List[float] = []
            idx: List[int] = []

            if not cls._valid_extension(a_file):
                continue

            for g in groups:
                f1, f2 = g[0].file_name, a_file.file_name

                f1 = clean_re.sub("", f1)
                f2 = clean_re.sub("", f2)
                r: float = l.ratio(f1, f2)

                if r > ratio:
                    comp.append(r)
                    idx.append(groups.index(g))

            if len(comp) > 0:
                best_match = comp.index(max(comp))
                groups[idx[best_match]].append(a_file)

            else:
                groups.append([a_file])

        if len(groups) < min_group_count and ratio < 1:
            return cls.group(files, ratio=ratio + 0.01, min_group_count=min_group_count)

        return groups


class SingleGrouping(ConditionGrouping):
    """Put all Pride files into own group."""

    @classmethod
    def group(cls, files: Sequence[PrideFile]) -> List[List[PrideFile]]:
        """Put all Pride files into onw group.

        Args:
            files (Sequence[PrideFile]): List of Pride files

        Returns:
            List[List[PrideFile]]: A list of groups of Pride files
        """
        return [[f for f in files if cls._valid_extension(f)]]


GROUPING_METHODS: Dict[str, Type[ConditionGrouping]] = {
    "single": SingleGrouping,
    "lev": LevGrouping,
    "simple": SimpleGrouping,
}
