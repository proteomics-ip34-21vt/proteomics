"""Proteomics StringDB cli."""

import argparse
import os
from typing import List, Optional

from proteomics.constants import VERSION
from proteomics.stringdb_interface.start_string import StringStarter


def string_cli(call_args: Optional[List[str]] = None) -> None:
    """Make a request to the StringDB and save the enrichment table and the image for given file.

    Args:
        call_args (Optional[List[str]], optional): Arguments passed by the call. Defaults to None.
    """
    parser = argparse.ArgumentParser(prog="stringDB")
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"Proteomics version {VERSION}",
        help="Prints out the current version",
    )

    parser.add_argument(
        "--species_id",
        metavar="species_id",
        type=int,
        required=True,
        help="Taxonomy id of the species.",
    )

    parser.add_argument(
        "--input_file",
        metavar="input_file",
        type=str,
        required=True,
        help="Path to PairwiseComparison.csv file.",
    )

    parser.add_argument(
        "--output_dir",
        metavar="output_dir",
        type=str,
        required=True,
        help="Existing output directory for enrichment table and plot.",
    )

    args = parser.parse_args(call_args)

    string_starter = StringStarter(args.species_id)
    string_starter.make_request(
        input_file=args.input_file,
        enrichment_path=os.path.join(args.output_dir, "enrichment_table.xlsx"),
        plot_path=os.path.join(args.output_dir, "stringdb_image.png"),
    )


if __name__ == "__main__":
    string_cli()
