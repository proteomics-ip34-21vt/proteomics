"""Start downstream string-db analysis from a MSStats outputfile."""
import logging
import re
from typing import Set

import pandas as pd

from proteomics.stringdb_interface.string_interface import StringRequest

logger = logging.getLogger(__name__)


class StringStarter:
    """Perform a request to the string-db, build from MSStats outputfile."""

    def __init__(self, species: int, pvalue_threshold: float = 0.05) -> None:
        """Init species as taxonomy id and the pvalue.

        Args:
            species (int): taxonomy id of the species
            pvalue_threshold (float, optional): pvalue, proteins with smaller pvalue will be used. Defaults to 0.05.
        """
        self.request: StringRequest = StringRequest()
        self.pvalue_threshold: float = pvalue_threshold
        self.species: int = species

    def collect_proteins(self, input_file: str) -> Set[str]:
        """Collect all proteinnames in a set from file, with pvalue greater than threshold.

        Args:
            input_file (str): full path of .csv MSStats outputfile

        Returns:
            Set[str]: Set of all proteinnames which have a pvalue greater than threshold.
        """
        msstats_df: pd.DataFrame = pd.read_csv(input_file)
        msstats_df = msstats_df.loc[msstats_df["pvalue"] < self.pvalue_threshold]
        protein_names: Set[str] = set(msstats_df["Protein"].unique())
        return protein_names

    def clear_names(self, protein_list: Set[str]) -> Set[str]:
        """Extract a protein-sequence from a string.

        Args:
            protein_list (Set[str]): Set of all the proteinnames

        Returns:
            Set[str]: Set with proteinnames, which are shortend to only letters and numbers
        """
        new_protein_list: Set[str] = set()

        no_spaces = re.compile(r"\s+")
        alphabetical_lead = re.compile(r"[a-zA-Z0-9]+")

        for protein in protein_list:
            # remove all white spaces
            full_name: str = re.sub(no_spaces, "", protein)
            # remove uniprot prefix
            names = full_name.split("|")
            names.remove("sp")

            # extract first part, made out of letters and numbers
            if len(names) > 0:
                name: str = names[0]
                search = re.search(alphabetical_lead, name)
                if search is not None:
                    stop_idx = search.end()
                    name = name[0:stop_idx]
                    if name:
                        new_protein_list.add(name)
        return new_protein_list

    def make_request(
        self, input_file: str, enrichment_path: str, plot_path: str
    ) -> None:
        """Make a request to the string-db and saves the enrichment table and the image.

        Args:
            input_file (str): output path from MSStats ('example.csv')
            enrichment_path (str): target path, where the enrichment table should be saved. ('example.xlsx')
            plot_path (str): target path, where the image should be saved. ('example.png')
        """
        protein_list = self.clear_names(self.collect_proteins(input_file))
        params = self.request.get_params(
            protein_list=protein_list, species=self.species
        )
        info = f"Make request for species: {self.species},\nproteinlist: {protein_list}"
        logger.info(info)
        self.request.get_enrichment(
            params=params, write_to_excel=True, target=enrichment_path
        )
        self.request.get_image(params=params, target=plot_path)
