"""Provides access to the String DB."""
import logging
from typing import Dict, Set, Union

import pandas as pd
import requests

logger = logging.getLogger(__name__)


class StringRequest:
    """An interface for the String DB."""

    STRING_API_URL: str = "https://version-11-5.string-db.org/api"
    IDENTITY: str = "Proteomics"

    def get_params(
        self, protein_list: Set[str], species: int
    ) -> Dict[str, Union[str, int]]:
        """Make default params for a requests and converts the proteins to the preferred proteinnames from String DB.

        Args:
            protein_list (Set[str]): A set of protein names
            species (int): the taxonomy id of the species

        Returns:
            Dict[str, Union[str, int]]: A dictionary, which can be used as data for the requests
        """
        params: Dict[str, Union[str, int]] = {
            "identifiers": "\r".join(protein_list),
            "species": species,
            "caller_identity": StringRequest.IDENTITY,
        }
        protein_list = self.get_string_names(params=params)
        params["identifiers"] = "\r".join(protein_list)
        return params

    def _str_to_df(self, txt: str, dropnan: bool = True) -> pd.DataFrame:
        """Convert string in tsv structure to pd.DataFrame.

        Args:
            txt (str): a string in tsv structure
            dropnan (bool): if empty rows should be dropped, default is True

        Returns:
            pd.DataFrame: A pd.DataFrame constructed from the txt, with colum names set
        """
        df: pd.DataFrame = pd.DataFrame([x.split("\t") for x in txt.split("\n")])
        df.columns = df.iloc[0]
        df = df.drop(0)
        if dropnan:
            df = df.dropna()
        return df

    def get_enrichment(
        self,
        params: Dict[str, Union[str, int]],
        write_to_excel: bool = False,
        target: str = "enrichment.xlsx",
    ) -> pd.DataFrame:
        """Make a post request to the string db to get the enrichment table and stores it in a pd.DataFrame.

        Args:
            params (Dict[str, Union[str, int]]): Dict with the params for the enrichment request
            write_to_csv (bool): if the DataFrame should be saved in a csv file, default is False
            target (str): filename for the csv file, only useful if write_to_csv=True, default is: "enrichment.csv"

        Returns:
            pd.DataFrame: A pd.DataFrame which contains the table
        """
        request_url: str = "/".join([StringRequest.STRING_API_URL, "tsv", "enrichment"])
        response: str = requests.post(request_url, data=params).text
        df: pd.DataFrame = self._str_to_df(txt=response)
        logger.info("Write to file: %s.", target)
        if write_to_excel:
            df.to_excel(excel_writer=target, index=False)
        return df

    def get_image(
        self, params: Dict[str, Union[str, int]], target: str = "plot.png"
    ) -> None:
        """Make a post request to the string db to get the image of the network.

        Args:
            params (Dict[str, Union[str, int]]): Dict with the params for the image request
            target (str): filename for the png file, default is: "plot.png"

        Returns:
            None
        """
        request_url: str = "/".join([StringRequest.STRING_API_URL, "image", "network"])
        response: bytes = requests.post(request_url, data=params).content
        logger.info("Write to file: %s.", target)
        with open(target, "wb") as fd:
            fd.write(response)

    def _map_to_strdb(self, params: Dict[str, Union[str, int]]) -> pd.DataFrame:
        """Map given proteins to String DB standards.

        Args:
            params (Dict[str, Union[str, int]]): Dict with the params for the map request

        Returns:
            (pd.DataFrame): An DataFrame which contains the response
        """
        request_url: str = "/".join(
            [StringRequest.STRING_API_URL, "tsv", "get_string_ids"]
        )
        response: str = requests.post(request_url, data=params).text
        df: pd.DataFrame = self._str_to_df(txt=response)
        return df

    def get_string_names(self, params: Dict[str, Union[str, int]]) -> Set[str]:
        """Transform a set of given proteins to String DB standard names.

        Args:
            params (Dict): Dict with the params for the map request

        Returns:
            (Set[str]): A List which contains all transformed protein names.
        """
        df = self._map_to_strdb(params)
        new_names: Set[str] = set(df["preferredName"].unique())
        return new_names
