"""A CLI interface to create a minimal Subcommand."""

import argparse
import os
import uuid
from typing import List

from proteomics.cli.helpers import StoreDictKeyPair, does_file_exist
from proteomics.cli.subcommand import Subcommand
from proteomics.exceptions import ProteomicsException
from proteomics.helpers.humanbytes import Humanbytes
from proteomics.helpers.nextflow_config import NextflowConfig
from proteomics.helpers.ram import installed_ram
from proteomics.helpers.run_config import RunConfig, RunState
from proteomics.helpers.uniprot_id_parser import get_proteome_id_by_orga_id


class MinimalSubcommand(Subcommand):
    """Minimal cli Subcommand, used to build new ones. Not for excecution made."""

    @classmethod
    def _add_arguments(cls, parser: argparse.ArgumentParser) -> None:
        """Add additional arguments to the parser.

        Args:
            parser (argparse.ArgumentParser): The argumentparser from Argparse.
        """
        available_ram = installed_ram()
        ms_fragger_default_ram = Humanbytes.to_javabytes(available_ram - Humanbytes.GB)

        parser.add_argument(
            "--name",
            metavar="name",
            type=str,
            required=True,
            help="Project name",
        )

        parser.add_argument(
            "--ram",
            metavar="ram",
            type=str,
            default=ms_fragger_default_ram,
            help=f"Configure how much ram MsFragger can use. Default on this machine: {ms_fragger_default_ram}.",
        )

        parser.add_argument(
            "--output_dir",
            metavar="output_dir",
            type=str,
            required=True,
            help="Target empty directory for output (will be created if it does not exist).",
        )

        parser.add_argument(
            "--custom_fasta_file",
            metavar="custom_fasta_file",
            type=str,
            default=None,
            help="Custom fasta database file for philosopher.",
        )

        parser.add_argument(
            "--downstream",
            action="store_true",
            help="If downstream analysis should be executed (if possible). Grouping is mandatory. Default is False.",
        )

        parser.add_argument(
            "--options",
            type=str,
            action=StoreDictKeyPair,
            help="Specify pipeline parameters. For example: annotation=10 (for setting the tmt version).",
            metavar="KEY1=VAL1,KEY2=VAL2...",
        )

    @classmethod
    def _get_name(cls) -> str:
        """Return the classname.

        Returns:
            str: minimal
        """
        return "minimal"

    @classmethod
    def _get_help(cls) -> str:
        """Return a short description.

        Returns:
            str: Proteomics minimal initialization.
        """
        return "Protemics minimal initialization"

    @classmethod
    def get_uniprot_id(cls, species_id: int) -> str:
        """Return the Uniprot DB id, for given species.

        Args:
            species_id (int): the taxonomy id of the species

        Raises:
            ProteomicsException: If no uniprot db id was found.

        Returns:
            str: the uniprot db id
        """
        uniprot_id = get_proteome_id_by_orga_id(species_id)
        if uniprot_id is None:
            raise ProteomicsException("Could not find Uniprot ID for this species.")
        return uniprot_id

    @classmethod
    def _run(cls, args: argparse.Namespace) -> List[RunConfig]:
        """Initialize the RunConfig.

        Args:
            args (argparse.Namespace): arguments by argparse

        Raises:
            ValueError: Output folder isn't empty or a non-existing folder.
            ProteomicsException: You tried to assign too much memory.
            ProteomicsException: Invalid java ram input value.
            ValueError: Could not find custom fasta file.

        Returns:
            List[RunConfig]: A list with the initialized RunConfig.
        """
        out_path: str = os.path.abspath(args.output_dir)
        if os.path.isdir(out_path):
            if len(os.listdir(out_path)) > 0:
                raise ValueError(
                    "Expected an empty folder or a non-existing foldername"
                )
        else:
            os.makedirs(out_path, exist_ok=False)

        try:
            ram = Humanbytes.from_javabytes(args.ram)
            i_ram = installed_ram()
            if ram > i_ram:
                raise ProteomicsException(
                    "You tried to allocate more ram than is available on this system ",
                    f"({Humanbytes.to_javabytes(ram)} / max {Humanbytes.to_javabytes(i_ram)})",
                )
        except ValueError as e:
            raise ProteomicsException("Invalid java ram input value") from e

        if args.custom_fasta_file is not None:
            if not does_file_exist(args.custom_fasta_file):
                raise ValueError(f"could not find {args.custom_fasta_file}")
            args.custom_fasta_file = os.path.abspath(args.custom_fasta_file)

        run_conf: RunConfig = RunConfig(
            config_id=str(uuid.uuid4()),
            name=args.name,
            output_dir=os.path.abspath(args.output_dir),
            input_dir="",
            species_id=0,
            uniprot_id="",
            custom_fasta_file=args.custom_fasta_file,
            downstream=args.downstream,
            keywords=[],
            grouping_method="single",
            remove_input=False,
            workflow=None,
            default_quantifiaction_method=None,
            nextflow_config=NextflowConfig(
                pipeline=None, ram=args.ram, options=args.options
            ),
            state=RunState.CREATED,
        )
        if run_conf.nextflow_config.options is None:
            run_conf.nextflow_config.options = {}
        return [run_conf]
