"""Proteomics CLI interface."""

import argparse
import logging
import os
from shutil import rmtree
from typing import List, Optional

from proteomics.cli.local_command import LocalSubcommand
from proteomics.cli.minimal_command import MinimalSubcommand
from proteomics.cli.pride_command import PrideSubcommand
from proteomics.constants import VERSION
from proteomics.helpers.ext_database import ExtDatabase
from proteomics.helpers.nf_command import NfCommand
from proteomics.helpers.run_config import RunConfig

logger = logging.getLogger(__name__)


def cli(call_args: Optional[List[str]] = None) -> None:
    """Start DB setup and Cli.

    Args:
        call_args (Optional[List[str]], optional): Arguments passed by the call. Defaults to None.

    Raises:
        e: Throw exception if no subcommand fits the call.
    """
    parser = argparse.ArgumentParser(prog="proteomics")
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"Proteomics version {VERSION}",
        help="Prints out the current version",
    )

    subparser = parser.add_subparsers()

    MinimalSubcommand.install_parser(subparser)
    LocalSubcommand.install_parser(subparser)
    PrideSubcommand.install_parser(subparser)

    args = parser.parse_args(call_args)

    run_confs: List[RunConfig] = []

    if hasattr(args, "func"):
        try:
            ExtDatabase.check_and_create_dbs()
            run_confs = args.func(args)
        except AttributeError as e:
            args.parser.print_help()
            raise e
    else:
        # General help. No command supplied
        parser.print_help()

    for run_config in run_confs:
        # execute workflow
        NfCommand.execute_cmd(run_config)
        if run_config.remove_input and os.path.exists(run_config.input_dir):
            logger.info("Remove folder: %s", run_config.input_dir)
            rmtree(run_config.input_dir)


if __name__ == "__main__":
    cli()
