"""CLI helpers."""

import argparse
import os
from typing import Any, List, Optional, Sequence, Union

from proteomics.exceptions import ProteomicsException
from proteomics.helpers.workflow import Workflow, all_workflows, find_workflow


class StoreDictKeyPair(argparse.Action):
    """Split up arguments and save them to a dictionary in an argparse action.

    For example "key1=value1,key2=value2" would lead to {"key1": "value1", "key2": "value2"}.
    """

    # pylint: disable=unused-argument
    def __call__(
        self,
        parser: argparse.ArgumentParser,
        namespace: argparse.Namespace,
        values: Union[str, Sequence[Any], None],
        option_string: Optional[str] = None,
    ) -> None:
        """Split up a provided key-value argument and set them in the argument dictionary."""
        if not isinstance(
            values, str
        ):  # because it must be a string, otherwise there is no sense in splitting it
            raise ValueError("Expected a string for the values.")

        my_dict = {}
        for key_value in values.split(","):
            key, value = key_value.split("=")
            my_dict[key] = value
        setattr(namespace, self.dest, my_dict)


def test_single_grouping(input_dir: str) -> bool:
    """Test if input directory contains more than one group.

    Args:
        input_dir (str): path of the input directory

    Returns:
        bool: if number of groups > 1
    """
    input_dir_path = os.path.abspath(input_dir)
    parent_dir = os.listdir(input_dir_path)
    return len(parent_dir) == 1


def test_folder_structure(input_dir: str) -> bool:
    """Test if input directory has just group-directories with just .raw/.mzMl files inside.

    Args:
        input_dir (str): path of the input directory

    Raises:
        ProteomicsException: Raised if other filetypes are found.

    Returns:
        bool: if the folder structure fullfilles our preconditions.
    """
    if not test_if_input(input_dir):
        return False
    input_dir_path = os.path.abspath(input_dir)
    parent_dir = os.listdir(input_dir_path)
    for directory in parent_dir:
        dir_path = os.path.join(input_dir_path, directory)
        if os.path.isfile(dir_path):
            return False
        group_dirs = os.listdir(dir_path)
        if len(group_dirs) < 1:
            return False
        for file in group_dirs:
            file_path = os.path.join(dir_path, file)
            if not os.path.isfile(file_path):
                return False
            if not (file.endswith(".raw") or file.endswith(".mzML")):
                raise ProteomicsException(
                    "invalid input file type, accepted are: .raw and .mzML"
                )
    return True


def test_if_input(input_dir: str) -> bool:
    """Test if there is any input.

    Args:
        input_dir (str): path of the input directory

    Returns:
        bool: Return True if directory is not empty.
    """
    try:
        input_dir_path = os.path.abspath(input_dir)
        parent_dir = os.listdir(input_dir_path)
        return len(parent_dir) > 0
    except FileNotFoundError:
        return False


def test_valid_workflow(name: str) -> Workflow:
    """Test if exactly one workflow is identified.

    Args:
        name (str): name of the workflow

    Raises:
        ValueError: if no workflows are found
        ProteomicsException: if multiple fitting workflows are detected

    Returns:
        Workflow: Return the fitting workflow.
    """
    workflows = find_workflow(name)
    if len(workflows) < 1:
        valid_names = get_git_url_names()
        raise ValueError(f"Invalid workflow name, available are: {valid_names}.")
    if len(workflows) > 1:
        raise ProteomicsException("multiple possible workflows detected")
    return workflows[0]


def does_file_exist(file_name: Optional[str]) -> bool:
    """Check if a fasta file exitst.

    Args:
        file_name (str): path to the file, which should be tested

    Returns:
        bool: if the fasta file is found
    """
    exists: bool = False
    if file_name is not None:
        exists = os.path.isfile(os.path.normpath(file_name))
    return exists


def get_git_url_names() -> List[str]:
    """Return a list of all git url names.

    Returns:
        List[str]: All names in a list.
    """
    names = []
    for workflow in all_workflows:
        wf_obj = workflow()
        for url in wf_obj.get_git_urls():
            names.append(url.name)
    return names
