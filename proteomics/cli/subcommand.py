"""Subcommand module."""

import abc
import argparse
from typing import List, cast

from proteomics.helpers.run_config import RunConfig


class Subcommand(abc.ABC):
    """Subcommand abstract class. Subclass to add new subcommands to the proteomics cli interface."""

    @classmethod
    def install_parser(
        cls, subparser: argparse._SubParsersAction  # type: ignore
    ) -> argparse.ArgumentParser:
        """Get the parser for the given subparser. Will also add all it's available arguments to the command.

        Args:
            subparser (argparse._SubParsersAction): The argument subparser to hook into

        Returns:
            argparse.ArgumentParser: The argument parser with the arguments added
        """
        parser = cast(
            argparse.ArgumentParser,
            subparser.add_parser(cls._get_name(), help=cls._get_help()),
        )
        cls._add_arguments(parser)
        parser.set_defaults(func=cls._run, parser=parser)
        return parser

    @classmethod
    @abc.abstractmethod
    def _run(cls, args: argparse.Namespace) -> List[RunConfig]:
        """Will be called if the cli interface determines that our subcommand shall be run.

        Args:
            args (argparse.Namespace): The arguments the subcommand was called with.
        """
        ...

    @classmethod
    @abc.abstractmethod
    def _add_arguments(cls, parser: argparse.ArgumentParser) -> None:
        """Add a parsers aruments.

        Args:
            parser (argparse.ArgumentParser): The parser to add the arguments to.
        """
        ...

    @classmethod
    @abc.abstractmethod
    def _get_name(cls) -> str:
        """Get the subcommand name.

        Returns:
            str: The subcommand name.
        """
        ...

    @classmethod
    @abc.abstractmethod
    def _get_help(cls) -> str:
        """Get the help string.

        Returns:
            str: CLI help string.
        """
        ...
