"""A CLI interface to call a pride Subcommand."""

import argparse
import logging
import os
from tempfile import mkdtemp
from typing import List

from pride_api.download_file import FTPDownloader
from pride_api.endpoint import files, projects
from pride_api.pride_objects import PrideFile, PrideProject

from proteomics.cli.helpers import test_folder_structure
from proteomics.cli.minimal_command import MinimalSubcommand
from proteomics.exceptions import ProteomicsException
from proteomics.helpers.run_config import RunConfig, RunState
from proteomics.helpers.workflow import find_workflow, find_workflow_for_project

logger = logging.getLogger(__name__)


class PrideSubcommand(MinimalSubcommand):
    """Minimal cli Subcommand."""

    @classmethod
    def _add_arguments(cls, parser: argparse.ArgumentParser) -> None:
        """Add additional arguments to the parser.

        Args:
            parser (argparse.ArgumentParser): The argumentparser from Argparse.
        """
        super()._add_arguments(parser)

        parser.add_argument(
            "--keywords",
            metavar="keywords",
            type=str,
            required=True,
            nargs="+",
            help="Pride IDs or other keywords which should be looked for in the PRIDE DB. Example: PXD024072 cancer mice",
        )

        parser.add_argument(
            "--grouping_method",
            metavar="grouping_method",
            type=str,
            required=True,
            help="With which concepts the grouping should be done. Available are: single, simple, lev.",
        )

        # pylint: disable=line-too-long
        parser.add_argument(
            "--default_quantification_method",
            metavar="default_quantification_method",
            type=str,
            default=None,
            help="Default quantification method which should be chosen, if the quantification method is empty. Use only with concrete Pride IDs as keywords! Available are: labelfree, open_search, tmt.",
        )

        # pylint: disable=line-too-long
        parser.add_argument(
            "--keep_input",
            action="store_false",
            help="If the flag is set, your input data won't be removed after processing.",
        )

    @classmethod
    def _get_name(cls) -> str:
        """Return the classname.

        Returns:
            str: pride
        """
        return "pride"

    @classmethod
    def _get_help(cls) -> str:
        """Return a short description.

        Returns:
            str: Proteomics pride interface
        """
        return "Protemics pride interface"

    @classmethod
    def _run(cls, args: argparse.Namespace) -> List[RunConfig]:
        """Build RunConfigs for pride projects.

        Args:
            args (argparse.Namespace): arguments ba argparser

        Returns:
            List[RunConfig]: list of built RunConfigs
        """
        run_confs: List[RunConfig] = super()._run(args)
        run_conf = run_confs[0]
        run_conf.remove_input = args.keep_input
        run_conf.keywords = args.keywords
        run_conf.grouping_method = args.grouping_method
        run_conf.default_quantifiaction_method = args.default_quantification_method
        run_confs = cls._generate_runconfs(run_conf)
        return run_confs

    @classmethod
    def _cancel_run(cls, run_config: RunConfig, message: str) -> None:
        """Cancel the run and log a message.

        Args:
            run_config (RunConfig): The RunConfig which is changed.
            message (str): logger message as warning.
        """
        logger.warning(message)
        logger.warning("Cancelled project: %s.", run_config.name)
        run_config.state = RunState.CANCELLED

    @classmethod
    def _generate_runconfs(cls, run_conf: RunConfig) -> List[RunConfig]:
        """Prepare and generate a RunConfig for each found pride project and download the data.

        Args:
            run_conf (RunConfig): The initial RunConfig.

        Raises:
            ProteomicsException: _description_
            ProteomicsException: _description_

        Returns:
            List[RunConfig]: _description_
        """
        request = projects.project_search(run_conf.keywords)
        run_confs: List[RunConfig] = []
        grouping_method = run_conf.get_grouping_method()
        if grouping_method is None:
            raise ProteomicsException(
                f"could not map {run_conf.grouping_method} to a grouping method."
            )

        if request.pagination is None:
            raise ProteomicsException(
                "The request does not contain a pagination object. Exiting."
            )

        while request.pagination.has_next_page():
            project_data: List[PrideProject] = next(request)
            for project in project_data:
                # set name
                pride_conf: RunConfig = run_conf.copy(mark_obsolete=False)
                run_confs.append(pride_conf)

                # download by accession for more detailed information
                pride_conf.name = project.accession
                logger.info("Create RunConfig for project: %s", pride_conf.name)
                project = next(projects.project_accession(pride_conf.name))[0]

                # check and set species_id
                pride_conf.species_id = project.organisms[0].accession
                if pride_conf.species_id is None:
                    cls._cancel_run(
                        pride_conf,
                        f"Could not find species id for project: {pride_conf.name}.",
                    )
                    continue
                logger.info("Selected species: %s", pride_conf.species_id)

                # check and set uniprot db id
                pride_conf.uniprot_id = cls.get_uniprot_id(pride_conf.species_id)
                logger.info("Selected proteomes-db id: %s", pride_conf.uniprot_id)

                # check and set workflow and pipeline
                workflows = find_workflow_for_project(project)
                if len(workflows) != 1:
                    if run_conf.default_quantifiaction_method is None:
                        cls._cancel_run(
                            pride_conf,
                            f"Could not identify a workflow for project: {pride_conf.name}.",
                        )
                        continue
                    pride_conf.workflow = find_workflow(
                        run_conf.default_quantifiaction_method
                    )[0]
                    logger.warning(
                        "Use default quantification method: %s, for project: %s",
                        pride_conf.default_quantifiaction_method,
                        pride_conf.name,
                    )
                else:
                    pride_conf.workflow = workflows[0]

                pride_conf.nextflow_config.pipeline = pride_conf.workflow.get_pipeline()
                logger.info(
                    "Selected workflow pipeline: %s", pride_conf.workflow.get_pipeline()
                )

                try:
                    search_for_tmt_txt = (
                        project.sample_processing_protocol + project.project_description
                    )
                    pride_conf.workflow.set_optional_params(
                        run_config=pride_conf, optional=[search_for_tmt_txt]
                    )
                    pride_conf.workflow.check_optional_params(pride_conf)
                except ProteomicsException:
                    cls._cancel_run(
                        pride_conf,
                        f"Could not identify a unique tmt version for project: {pride_conf.name}.",
                    )
                    continue

                # create input dir and subfolders for grouping
                cls._download_input(pride_conf)

                # test input folder
                if not test_folder_structure(pride_conf.input_dir):
                    cls._cancel_run(
                        pride_conf,
                        f"Failed to download files for project: {pride_conf.name}.",
                    )
                    continue

                # make and set output_dir
                pride_conf.output_dir = os.path.join(
                    run_conf.output_dir, pride_conf.name
                )
                os.makedirs(pride_conf.output_dir, exist_ok=False)
                logger.info("Created output directory: %s", pride_conf.output_dir)

        return run_confs

    @classmethod
    def _download_input(cls, run_conf: RunConfig) -> None:
        """Create input folder structure and download the input files.

        Args:
            run_conf (RunConfig): the according RunConfig

        Raises:
            ProteomicsException: If grouping_method is None.
        """
        # create input dir and subfolders for grouping
        # pylint: disable=consider-using-with
        run_conf.input_dir = mkdtemp(prefix=f"proteomics_{run_conf.name}_")
        logger.info("Created input directory: %s", run_conf.input_dir)

        # gather project files
        prj_files: List[PrideFile] = next(files.get_files_by_project(run_conf.name))
        to_download: List[PrideFile] = []
        # select .raw and .mzML files
        for prj_file in prj_files:
            if prj_file.file_name.endswith(".raw") or prj_file.file_name.endswith(
                "mzML"
            ):
                to_download.append(prj_file)

        grouping_method = run_conf.get_grouping_method()

        if grouping_method is None:
            raise ProteomicsException(
                f"could not map {grouping_method} to a grouping method."
            )

        grouped_prj_files = grouping_method().group(to_download)

        for idx, group in enumerate(grouped_prj_files):
            # declare group folder
            group_folder = os.path.join(run_conf.input_dir, "group_" + str(idx))
            # download group
            downloader = FTPDownloader(group_folder)
            logger.info("Created group folder: %s.", group_folder)
            logger.info("Downloading experiment data...")
            downloader.download_ftp_files_threaded(group)
