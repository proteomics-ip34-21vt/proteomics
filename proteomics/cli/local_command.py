"""A CLI interface to call a local Subcommand."""

import argparse
import logging
import os
from typing import List

from proteomics.cli.helpers import test_folder_structure, test_single_grouping
from proteomics.cli.helpers import test_valid_workflow
from proteomics.cli.minimal_command import MinimalSubcommand
from proteomics.exceptions import ProteomicsException
from proteomics.helpers.run_config import RunConfig

logger = logging.getLogger(__name__)


class LocalSubcommand(MinimalSubcommand):
    """Minimal cli Subcommand."""

    @classmethod
    def _add_arguments(cls, parser: argparse.ArgumentParser) -> None:
        """Add additional arguments to the parser.

        Args:
            parser (argparse.ArgumentParser): The argumentparser from Argparse.
        """
        super()._add_arguments(parser)

        parser.add_argument(
            "--input_dir",
            metavar="input_dir",
            type=str,
            required=True,
            help="Input folder, with grouping structure",
        )

        parser.add_argument(
            "--species_id",
            metavar="species_id",
            type=int,
            required=True,
            help="Taxonomy id of the species.",
        )

        parser.add_argument(
            "--workflow",
            metavar="workflow",
            type=str,
            required=True,
            help="Nextflow workflow. Either a local file or one of [labelfree, open_search, tmt]",
        )

    @classmethod
    def _get_name(cls) -> str:
        """Return the classname.

        Returns:
            str: local
        """
        return "local"

    @classmethod
    def _get_help(cls) -> str:
        """Return a short description.

        Returns:
            str: Proteomics local interface
        """
        return "Proteomics local interface"

    @classmethod
    def _run(cls, args: argparse.Namespace) -> List[RunConfig]:
        """Build a RunConfig for a local run.

        Args:
            args (argparse.Namespace): arguments by argparse

        Raises:
            ProteomicsException: Wrong folder structure (one folder per group)
            ProteomicsException: Could not identify a uniprot db ID for this species.

        Returns:
            List[RunConfig]: A list, which contains the RunConfig. (Local always exactly one.)
        """
        run_conf: RunConfig = super()._run(args)[0]

        if not test_folder_structure(args.input_dir):
            raise ProteomicsException("Invalid input folder structure.")

        if run_conf.downstream:
            if test_single_grouping(args.input_dir):
                logger.warning("downstream analysis: single grouping detected")

        workflow = test_valid_workflow(args.workflow)

        run_conf.workflow = workflow
        run_conf.nextflow_config.pipeline = workflow.get_pipeline()
        run_conf.input_dir = os.path.abspath(args.input_dir)
        run_conf.species_id = args.species_id
        run_conf.uniprot_id = cls.get_uniprot_id(args.species_id)

        logger.info("Selected workflow pipeline: %s", run_conf.nextflow_config.pipeline)
        return [run_conf]
